# GPU Feature Discovery

# MIGRATION NOTICE

With the [v0.15.0](https://github.com/NVIDIA/k8s-device-plugin/releases/tag/v0.15.0)
release of the NVIDIA GPU Device Plugin and GPU Feature Discovery, the migration
of development for GPU Feature Discovery to the
https://github.com/NVIDIA/k8s-device-plugin is complete.

This repository is archived and is only maintained for reference purposes.

## Issues and Contributing

[Checkout the Contributing document!](https://github.com/NVIDIA/k8s-device-plugin/blob/main/CONTRIBUTING.md)

For questions, feature requests, or bugs, open an issue against the [k8s-device-plugin](https://github.com/NVIDIA/k8s-device-plugin) repository.

## Contact

For questions or inquiries, please create an issue at [k8s-device-plugin](https://github.com/NVIDIA/k8s-device-plugin)
